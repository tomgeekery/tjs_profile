api = 2
core = 7.x

; Core
projects[] = drupal

; Contrib modules
projects[context][subdir] = contrib
projects[ctools][subdir] = contrib
projects[devel][subdir] = contrib
projects[diff][subdir] = contrib
projects[features][subdir] = contrib
projects[features_extra][subdir] = contrib
projects[strongarm][subdir] = contrib
projects[views][subdir] = contrib
projects[module_filter][subdir] = contrib
projects[admin_menu][subdir] = contrib
projects[token][subdir] = contrib
projects[pathauto][subdir] = contrib
projects[metatag][subdir] = contrib
projects[block_class][subdir] = contrib
projects[entity][subdir] = contrib
projects[globalredirect][subdir] = contrib
projects[adminimal_admin_menu][subdir] = contrib
projects[jquery_update][subdir] = contrib
projects[empty_front_page][subdir] = contrib
projects[draggableviews][subdir] = contrib
projects[libraries][subdir] = contrib
projects[flexslider][subdir] = contrib
projects[link][subdir] = contrib
projects[paragraphs][subdir] = contrib
projects[ckeditor][subdir] = contrib

; Custom modules
projects[custom][download][type] = "git"
projects[custom][download][url] = "https://tomgeekery@bitbucket.org/tomgeekery/custom.git"
projects[custom][type] = "module"
projects[custom][subdir] = "custom"

projects[contact_info][download][type] = "git"
projects[contact_info][download][url] ="https://tomgeekery@bitbucket.org/tomgeekery/contact_info.git"
projects[contact_info][type] = "module"
projects[contact_info][subdir] = "custom"

projects[tjs_front_page][download][type] = "git"
projects[tjs_front_page][download][url] = "https://tomgeekery@bitbucket.org/tomgeekery/tjs_front_page.git"
projects[tjs_front_page][type] = "module"
projects[tjs_front_page][subdir] = "custom"

; Profile
projects[tjs_profile][type] = "profile"
projects[tjs_profile][download][type] = "git"
projects[tjs_profile][download][url] = "https://tomgeekery@bitbucket.org/tomgeekery/tjs_profile.git"

; Libraries
libraries[flexslider][download][type] = "git"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider.git"
libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][destination] = "libraries"

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.0/ckeditor_4.4.0_standard.zip"
libraries[ckeditor][destination] = "libraries"
libraries[ckeditor][directory_name] = "ckeditor"

; Themes
projects[] = "adminimal_theme"
projects[zurb-foundation][patch][2205041] = "https://drupal.org/files/issues/zurb-foundation-array-2-string-2205041-6.patch"

projects[tjs_theme][type] = "theme"
projects[tjs_theme][download][type] = "git"
projects[tjs_theme][download][url] = "https://tomgeekery@bitbucket.org/tomgeekery/tjs_theme.git"
