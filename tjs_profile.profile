<?php

/**
 * @file
 * 
 */

/**
 * Implements hook_install_tasks().
 */
function tjs_profile_install_tasks($install_state) {
  $tasks['tjs_setup'] = array(
    'display_name' => st('Additional features'),
    'display' => TRUE,
    'type' => 'form',
    'run' => INSTALL_TASK_RUN_IF_REACHED,
    'function' => 'tjs_profile_configure_extra_options',
  );
  
  return $tasks;
}

/**
 * Setup social media
 */
function tjs_profile_configure_extra_options($form, &$form_state) {
  $form = array();

  $form['additional_features'] = array(
    '#type' => 'fieldset',
    '#title' => st('Additional features'),
  );

  $form['additional_features']['tjs_site_slideshow'] = array(
    '#type' => 'select',
    '#title' => st('Enable slideshow'),
    '#options' => array(
      TRUE => 'Yes',
      FALSE => 'No',
    ),
    '#required' => TRUE,
  );

  $form['additional_features']['tjs_site_blog'] = array(
    '#type' => 'select',
    '#title' => st('Enable blog'),
    '#options' => array(
      TRUE => 'Yes',
      FALSE => 'No',
    ),
    '#required' => TRUE,
  );

  $form['#submit'][] = 'tjs_profile_configure_extra_options_submit';

  return system_settings_form($form);
}

/**
 * Submit function for extra options form.
 */
function tjs_profile_configure_extra_options_submit($form, &$form_state) {
  $test = 1;
  if ($form_state['values']['tjs_site_slideshow']) {
    // Enable slideshow modules.
  }
  if ($form_state['values']['tjs_site_blog']) {
    // Enable blog modules.
  }
}
